

function convert() {
    var str = document.getElementById("birthday").value;
    if (str.length == 8) {

        var reggie = /(\d{4})(\d{2})(\d{2})/;
        var dateArray = reggie.exec(str);

        var dateObject = new Date(
            (+dateArray[1]),
            (+dateArray[2] - 1), // Careful, month starts at 0!
            (+dateArray[3]),
        );
        var Str1 = formatDate(dateObject);
        alert(dateObject);

        document.getElementById("birthday").value = Str1;
    }
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('/');
}

$(document).ready(function () {
    $('#birthday').blur(function () {
        convert();
    });
});